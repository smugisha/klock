package cmeon.qrockwidget;

import android.graphics.Color;
import android.widget.RemoteViews;

/**
 * Created by cmeon on 4/5/15.
 */
public class QStackGrid_NL extends QStackGrid {
    private static final int HALF = 30;

    public static class Lang implements QlockTime.WordTime {
        private final char[] mCharacters = (
                "HETKISAVIJFTIENAMZVOOR" +
                        "OVERPMKWARTHALFSBMOVER" +
                        "VOORTHGÉÉNSTWEEPVCDRIE" +
                        "VIERVIJFZESZEVENONEGEN" +
                        "ACHTTIENELFTWAALFBFUUR").toCharArray();

        @Override
        public RemoteViews time(RemoteViews rv, int position, int hour, int minute) {
            if (position < 3) {
                // Displays HET
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }

            else if (position > 3 && position <= 5) {
                // Displays IS
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }

            else {
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.BLACK);
            }

            // Displays minutes
            // Displays FIVE
            if ((minute > 4 && minute <= 9) || (minute > 54 && minute <= 59)) {
                if (position >= 7 && position <= 10) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TEN
            else if (minute > 9 && minute <= 14 || (minute > 49 && minute <= 54)) {
                if (position >= 11 && position <= 14) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays QUARTER
            else if (minute > 14 && minute <= 19) {
                if ((position >= 28 & position <= 32)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            else if (minute > 44 && minute <= 49) {
                if ((position >= 28 & position <= 32)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTY
            else if (minute > 19 && minute <= 24 || (minute > 39 && minute <= 44)) {
                if ( (position >= 11 && position <= 14) ||  (position >= 18 && position <= 21) ) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTYFIVE
            else if (minute > 24 && minute <= 29 || (minute > HALF && minute <= 39)) {
                if ( (position >= 7 && position <= 10) ||  (position >= 18 && position <= 21) ) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays HALF
            if (minute >= 24 && minute <= 39) {
                if (position >= 33 && position <= 36) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays OCLOCK
            else if (minute >= 0 && minute <= 4) {
                if (position >= 107 && position <= 109) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays PAST
            /*(minute > 14 && minute <= 19) || (minute > 44 && minute <= 49)*/
            if ( (minute > 4 && minute < 24)
                    || (minute > HALF && minute <= 39) ) {
                if (position >= 40 && position <= 43) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TO
            else if ((minute >= 24 && minute <= 29)
                    || (minute > 39 && minute < 60)) {
                if (position >= 33 && position <= 35) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays Hours
            // 1
            if ((hour == 1 && minute <= HALF) || (hour == 0 && minute > HALF)) {
                if (position >= 51 && position <= 53) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 2
            else if ((hour == 2 && minute <= 24) || (hour == 1 && minute > 24)) {
                if (position >= 55 && position <= 58) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 3
            else if ((hour == 3 && minute <= 24) || (hour == 2 && minute > 24)) {
                if (position >= 66 && position <= 69) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 4
            else if ((hour == 4 && minute <= 24) || (hour == 3 && minute > 24)) {
                if (position >= 62 && position <= 65) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 5
            else if ((hour == 5 && minute <= 24) || (hour == 4 && minute > 24)) {
                if (position >= 70 && position <= 73) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 6
            else if ((hour == 6 && minute <= 24) || (hour == 5 && minute > 24)) {
                if (position >= 74 && position <= 76) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 7
            else if ((hour == 7 && minute <= 24) || (hour == 6 && minute > 24)) {
                if (position >= 77 && position <= 81) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 8
            else if ((hour == 8 && minute <= 24) || (hour == 7 && minute > 24)) {
                if (position >= 88 && position <= 91) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 9
            else if ((hour == 9 && minute <= 24) || (hour == 8 && minute > 24)) {
                if (position >= 83 && position <= 87) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 10
            else if ((hour == 10 && minute <= 24) || (hour == 9 && minute > 24)) {
                if (position >= 92 && position <= 95) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 11
            else if ((hour == 11 && minute <= 24) || (hour == 10 && minute > 24)) {
                if (position >= 96 && position <= 98) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 12
            else if ((hour == 0 && minute <= 24) || (hour == 11 && minute > 24)) {
                if (position >= 99 && position <= 104) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            return rv;
        }

    }
}
