package cmeon.qrockwidget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class TimeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (Intent.ACTION_TIME_CHANGED.equals(action)
                || Intent.ACTION_TIMEZONE_CHANGED.equals(action)) {
            AppWidgetManager mAppWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName componentName = new ComponentName(
                    context,
                    Mandela.class);
            int[] mAppWidgetIds = mAppWidgetManager.getAppWidgetIds(componentName);
            mAppWidgetManager.notifyAppWidgetViewDataChanged(mAppWidgetIds, R.id.g_rock_grid2);
        }
    }
}
