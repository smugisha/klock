package cmeon.qrockwidget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import java.util.Calendar;

/**
 * Created by cmeon on 25/09/14.
 * Alarm to trigger update events for the clock widget.
 */
public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
    private long interval;
    private long triggerTime;

    public AlarmManagerBroadcastReceiver()
    {
        this.interval    = 5 * 1000;
        this.triggerTime = System.currentTimeMillis() + interval;
    }

    public AlarmManagerBroadcastReceiver (double intervalInSeconds, long triggerAfterMillSec) {
        this.interval    = (long)(intervalInSeconds * 1000);
        this.triggerTime = triggerAfterMillSec;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "YOUR TAG");
        wl.acquire();

        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        if ((minute % 5) == 0) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            ComponentName componentName = new ComponentName(context, Mandela.class);
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(componentName);

            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.g_rock_grid2);
        }

        wl.release();
    }

    public void setAlarm(Context context) {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        am.setRepeating(
                AlarmManager.RTC_WAKEUP, triggerTime, interval , pIntent);
    }

    public void cancelAlarm(Context context) {
        Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
}
