package cmeon.qrockwidget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

/**
 * Created by cmeon on 23/09/14.
 * Fills the grid with values based on the current time
 */
public class QStackGrid extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new QRockGridFactory(this.getApplicationContext(), intent);
    }

    public class QRockGridFactory implements RemoteViewsFactory {
        private static final int mCount = 110;
        private Context mContext;
        private QlockTime.Time time;
        private int appWidgetId;
        private String lang;

        public QRockGridFactory(Context applicationContext, Intent intent) {
            mContext = applicationContext;
            // create according to the language
            Bundle extras = intent.getExtras();
            appWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
            lang = extras.getString(MainActivity.LANG+"-"+appWidgetId);
            time = new QlockTime.Time(lang);
        }

        @Override
        public void onCreate() {
        }

        @Override
        public void onDataSetChanged() {
        }

        @Override
        public void onDestroy() {
        }

        @Override
        public int getCount() {
            return mCount;
        }

        @Override
        public RemoteViews getViewAt(int position) {

            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.g_rock_grid);
            return time.time(rv, position);

        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public int getViewTypeCount() {
            return mCount;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}