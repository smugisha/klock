package cmeon.qrockwidget;

import android.graphics.Color;
import android.widget.RemoteViews;

/**
 * Created by cmeon on 27/11/14.
 */
public class QStackGrid_DE extends QStackGrid {
    public static class Lang implements QlockTime.WordTime {
        private final char[] mCharacters = (
                "ESKISTAFÜNFZEHNZWANZIG" +
                        "DREIVIERTELVORFUNKNACH" +
                        "HALBAELFÜNFEINSXAMZWEI" +
                        "DREIPMJVIERSECHSNLACHT" +
                        "SIEBENZWÖLFZEHNEUNKUHR").toCharArray();

        @Override
        public RemoteViews time(RemoteViews rv, int position, int hour, int minute) {
            if (position < 2) {
                // Displays ES
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }

            else if (position > 2 && position <= 5) {
                // Displays IST
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }

            else {
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.BLACK);
            }

            // Displays minutes
            // Displays FIVE
            if ((minute > 4 && minute <= 9) || (minute > 54 && minute <= 59)) {
                if (position >= 7 && position <= 10) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TEN
            else if (minute > 9 && minute <= 14 || (minute > 49 && minute <= 54)) {
                if (position >= 11 && position <= 14) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays QUARTER
            else if (minute > 14 && minute <= 19) {
                if ((position >= 26 & position <= 32)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            else if (minute > 44 && minute <= 49) {
                if ((position >= 26 & position <= 32)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTY
            else if (minute > 19 && minute <= 24 || (minute > 39 && minute <= 44)) {
                if (position >= 15 && position <= 21) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTYFIVE
            else if (minute > 24 && minute <= 29 || (minute > QlockTime.HALF && minute <= 39)) {
                if (position >= 7 && position <= 10) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
                /*
                if ((position >= 22 && position <= 31)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
                */
            }

            // Displays HALF
            if (minute >= 24 && minute <= 39) {
                if (position >= 44 && position <= 47) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays OCLOCK
            else if (minute >= 0 && minute <= 4) {
                if (position >= 107 && position <= 109) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays PAST
            /*(minute > 14 && minute <= 19) || (minute > 44 && minute <= 49)*/
            if ( (minute > 4 && minute < 24)
                    || (minute > QlockTime.HALF && minute <= 39) ) {
                if (position >= 40 && position <= 43) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TO
            else if ((minute >= 24 && minute <= 29)
                    || (minute > 39 && minute < 60)) {
                if (position >= 33 && position <= 35) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays Hours
            // 1
            if (hour == 1 && minute <= 24) {
                if (position >= 55 && position <= 57) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            else if (hour == 0 && minute > 24) {
                if (position >= 55 && position <= 58) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 2
            else if ((hour == 2 && minute <= 24) || (hour == 1 && minute > 24)) {
                if (position >= 62 && position <= 65) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 3
            else if ((hour == 3 && minute <= 24) || (hour == 2 && minute > 24)) {
                if (position >= 66 && position <= 69) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 4
            else if ((hour == 4 && minute <= 24) || (hour == 3 && minute > 24)) {
                if (position >= 73 && position <= 76) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 5
            else if ((hour == 5 && minute <= 24) || (hour == 4 && minute > 24)) {
                if (position >= 51 && position <= 54) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 6
            else if ((hour == 6 && minute <= 24) || (hour == 5 && minute > 24)) {
                if (position >= 77 && position <= 81) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 7
            else if ((hour == 7 && minute <= 24) || (hour == 6 && minute > 24)) {
                if (position >= 88 && position <= 93) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 8
            else if ((hour == 8 && minute <= 24) || (hour == 7 && minute > 24)) {
                if (position >= 84 && position <= 87) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 9
            else if ((hour == 9 && minute <= 24) || (hour == 8 && minute > 24)) {
                if (position >= 102 && position <= 105) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 10
            else if ((hour == 10 && minute <= 24) || (hour == 9 && minute > 24)) {
                if (position >= 99 && position <= 102) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 11
            else if ((hour == 11 && minute <= 24) || (hour == 10 && minute > 24)) {
                if (position >= 49 && position <= 51) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 12
            else if ((hour == 0 && minute <= 24) || (hour == 11 && minute > 24)) {
                if (position >= 94 && position <= 98) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            return rv;
        }

    }
}
