package cmeon.qrockwidget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;

public class MainActivity extends Activity implements ColorPicker.OnColorChangedListener {

    public static final String LANG = "cmeon.qrockwidget.LANG";
    public static final String COLOR = "cmeon.qrockwidget.COLOR";
    private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    public static final String PREF_NAME = "QRockWidget";

    public MainActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the result to CANCELLED. This will cause the widget host to cancel
        // out of the widget placement if they press the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.activity_main);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID,  AppWidgetManager.INVALID_APPWIDGET_ID
            );
        }

        // If they gave an intent without an id, just bail.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }

        ColorPicker colorPicker = (ColorPicker) findViewById(R.id.color_picker);
        OpacityBar opacityBar   = (OpacityBar) findViewById(R.id.opacitybar);
        SVBar svBar             = (SVBar) findViewById(R.id.svbar);

        colorPicker.addSVBar(svBar);
        colorPicker.addOpacityBar(opacityBar);
        colorPicker.setOnColorChangedListener(this);

        // Configure app.
        Button addWidget = (Button) findViewById(R.id.add_button);
        addWidget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());

                SharedPreferences settings = getSharedPreferences(PREF_NAME, 0);
                int color = settings.getInt( (MainActivity.COLOR+"-"+mAppWidgetId), 0);
                String lang = settings.getString( (MainActivity.LANG+"-"+mAppWidgetId), "en");

                // Update AppWidget RemoteViews
                Mandela.updateAppWidget(getApplicationContext(), appWidgetManager, mAppWidgetId, color, lang);

                Intent resultValue = new Intent();
                resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                setResult(RESULT_OK, resultValue);
                finish();
            }
        });
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        SharedPreferences settings = getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.de:
                if (checked) {
                    // German language
                    editor.putString( (MainActivity.LANG+"-"+mAppWidgetId), "de");
                    editor.apply();
                    break;
                }
            case R.id.en:
                if (checked) {
                    // English language
                    editor.putString( (MainActivity.LANG+"-"+mAppWidgetId), "en");
                    editor.apply();
                    break;
                }
            case R.id.nl:
                if (checked) {
                    // Swahili language
                    editor.putString( (MainActivity.LANG+"-"+mAppWidgetId), "nl");
                    editor.apply();
                    break;
                }
            case R.id.sw:
                if (checked) {
                    // Swahili language
                    editor.putString( (MainActivity.LANG+"-"+mAppWidgetId), "sw");
                    editor.apply();
                    break;
                }
        }
    }

    @Override
    public void onColorChanged(int color) {
        SharedPreferences settings = getSharedPreferences(PREF_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt( (MainActivity.COLOR+"-"+mAppWidgetId), color);
        editor.apply();
    }
}
