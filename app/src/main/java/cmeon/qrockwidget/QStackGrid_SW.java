package cmeon.qrockwidget;

import android.graphics.Color;
import android.widget.RemoteViews;

/**
 * Created by cmeon on 27/11/14.
 */
public class QStackGrid_SW extends QStackGrid {
    public static class Lang implements QlockTime.WordTime {
        private final char[] mCharacters = (
                "NIWSAABTANOTATUNANENNE" +
                        "SABACHISITAKUMITISAMNA" +
                        "MOJAVKMBILINAHKASOROBO" +
                        "DAKIKATNUSUKUMIOIACMEO" +
                        "ISHIRINIZNATANOVKAMILI").toCharArray();

        @Override
        public RemoteViews time(RemoteViews rv, int position, int hour, int minute) {

            if (position < 2) {
                // Displays NI
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }

            else if (position > 2 && position <= 5) {
                // Displays IS
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.WHITE);
            }


            else {
                rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                rv.setTextColor(R.id.text, Color.BLACK);
            }

            // Displays FIVE
            if ((minute > 4 && minute <= 9) || (minute > 54 && minute <= 59)) {
                if (position >= 99 && position <= 102) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TEN
            else if (minute > 9 && minute <= 14 || (minute > 49 && minute <= 54)) {
                if (position >= 77 && position <= 80) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays QUARTER
            else if (minute > 14 && minute <= 19 || (minute > 44 && minute <= 49)) {
                if (position >= 62 & position <= 65) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTY
            else if (minute > 19 && minute <= 24 || (minute > 39 && minute <= 44)) {
                if (position >= 88 && position <= 95) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays TWENTYFIVE
            else if (minute > 24 && minute <= 29 || (minute > QlockTime.HALF && minute <= 39)) {
                if ((position >= 88 && position <= 95)
                        || (position >= 97 && position <= 98)
                        || (position >= 99 && position <= 102)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays HALF
            else if ( (minute >= 30 && minute <= QlockTime.HALF) ) {
                if (position >= 73 && position <= 76) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays OCLOCK
            else if (minute >= 0 && minute <= 4) {
                if (position >= 104 && position <= 109) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays NA
            if (minute > 4 && minute <= QlockTime.HALF) {
                if (position >= 55 && position <= 56) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays DAKIKA
            if (((minute > 4 && minute <= 29) || (minute > QlockTime.HALF && minute < 60)) && !((minute > 14 && minute <= 19) || (minute > 44 && minute <= 49))) {
                if (position >= 66 && position <= 71) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays KASORO
            if ((minute > QlockTime.HALF && minute < 60)) {
                if (position >= 58 && position <= 63) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // Displays Hours
            // 1
            if ((hour == 1 && minute <= QlockTime.HALF) || (hour == 0 && minute > QlockTime.HALF)) {
                if (position >= 22 && position <= 25) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 2
            else if ((hour == 2 && minute <= QlockTime.HALF) || (hour == 1 && minute > QlockTime.HALF)) {
                if (position >= 15 && position <= 18) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 3
            else if ((hour == 3 && minute <= QlockTime.HALF) || (hour == 2 && minute > QlockTime.HALF)) {
                if (position >= 37 && position <= 40) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 4
            else if ((hour == 4 && minute <= QlockTime.HALF) || (hour == 3 && minute > QlockTime.HALF)) {
                if (position >= 33 && position <= 36) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 5
            else if ((hour == 5 && minute <= QlockTime.HALF) || (hour == 4 && minute > QlockTime.HALF)) {
                if ((position >= 33 && position <= 36)
                        || (position >= 42 && position <= 43)
                        || (position >= 44 && position <= 47)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 6
            else if ((hour == 6 && minute <= QlockTime.HALF) || (hour == 5 && minute > QlockTime.HALF)) {
                if ((position >= 33 && position <= 36)
                        || (position >= 42 && position <= 43)
                        || (position >= 50 && position <= 54)) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 7
            else if ((hour == 7 && minute <= QlockTime.HALF) || (hour == 6 && minute > QlockTime.HALF)) {
                if (position >= 44 && position <= 47) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 8
            else if ((hour == 8 && minute <= QlockTime.HALF) || (hour == 7 && minute > QlockTime.HALF)) {
                if (position >= 50 && position <= 54) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 9
            else if ((hour == 9 && minute <= QlockTime.HALF) || (hour == 8 && minute > QlockTime.HALF)) {
                if (position >= 11 && position <= 14) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 10
            else if ((hour == 10 && minute <= QlockTime.HALF) || (hour == 9 && minute > QlockTime.HALF)) {
                if (position >= 19 && position <= 21) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 11
            else if ((hour == 11 && minute <= QlockTime.HALF) || (hour == 10 && minute > QlockTime.HALF)) {
                if (position >= 7 && position <= 10) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            // 12
            else if ((hour == 0 && minute <= QlockTime.HALF) || (hour == 11 && minute > QlockTime.HALF)) {
                if (position >= 29 && position <= 32) {
                    rv.setTextViewText(R.id.text, "" + mCharacters[position]);
                    rv.setTextColor(R.id.text, Color.WHITE);
                }
            }

            return rv;
        }

    }
}
