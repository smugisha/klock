package cmeon.qrockwidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.RemoteViews;


/**
 * Implementation of App Widget functionality.
 */
public class Mandela extends AppWidgetProvider {
    private AlarmManagerBroadcastReceiver am;

    @Override
    public void onUpdate(Context context,
                         AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            SharedPreferences settings = context.getSharedPreferences(MainActivity.PREF_NAME, 0);
            int color = settings.getInt( (MainActivity.COLOR+"-"+appWidgetId), 0);
            String lang = settings.getString( (MainActivity.LANG+"-"+appWidgetId), "en");
            updateAppWidget(context, appWidgetManager, appWidgetId, color, lang);
        }
    }

    @Override
    public void onEnabled(Context context) {
        am = new AlarmManagerBroadcastReceiver(60,
                System.currentTimeMillis()+(30*1000));
        am.setAlarm(context);
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
        am = new AlarmManagerBroadcastReceiver();
        am.cancelAlarm(context);
    }

    public static void updateAppWidget(
            Context context, AppWidgetManager appWidgetManager,
            int appWidgetId, int color, String lang) {
        // Construct the RemoteViews object

        Intent intent = new Intent (context, NewRemoteView.createRemoteViewService(lang));//QStackGrid.class);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        intent.putExtra(MainActivity.LANG+"-"+appWidgetId, lang);

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.mandela);

        views.setInt(R.id.g_rock_grid2, "setBackgroundColor", color);
        views.setRemoteAdapter(R.id.g_rock_grid2, intent);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }
}


