package cmeon.qrockwidget;

/**
 * Created by cmeon on 26/11/14.
 */

public class NewRemoteView {
    public static Class createRemoteViewService(String lang) {
        Class rvServiceClass;

        if (lang.equals("de")) {
            rvServiceClass = QStackGrid_DE.class;
        }
        else if (lang.equals("en")) {
            rvServiceClass = QStackGrid_EN.class;
        }
        else if (lang.equals("sw")) {
            rvServiceClass = QStackGrid_SW.class;
        }
        else if (lang.equals("nl")) {
            rvServiceClass = QStackGrid_NL.class;
        }
        else {
            rvServiceClass = QStackGrid_EN.class;
        }


        return rvServiceClass;
    }
}
